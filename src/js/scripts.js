(function ($, window, document, undefined) {
  "use strict";



  $(function () {
    jQuery('.popup-youtube').magnificPopup({
  		// disableOn: 700,
  		type: 'iframe',
  		mainClass: 'mfp-fade',
  		removalDelay: 160,
  		preloader: false,

  		fixedContentPos: false
  	});
    jQuery(".hero-slider").slick({
      infinite: false,
      speed: 2000,
      arrows: false,
      cssEase: "ease",
      autoplay: true,
      autoplaySpeed: 1500
    });
    jQuery(".navbar-nav > li > a").on("click", function () {
      jQuery(".navbar-nav")
        .find(".active")
        .removeClass("active");
      jQuery(this)
        .parent()
        .addClass("active");
    });
    jQuery('.pills-web .previ a').click(function () {
      if (!jQuery(".inner-banner").hasClass("preview")) {
        jQuery(".inner-banner").addClass("preview");
      } else {
        jQuery(".inner-banner").removeClass("preview");
      }
    });
    jQuery(".team-slider").slick({
      infinite: true,
      speed: 500,
      fade: false,
      cssEase: "linear",
      autoplay: true,
      autoplaySpeed: 2000
    });
    jQuery(".st-slider-shop").slick({
      infinite: false,
      speed: 1000,
      cssEase: "ease",
      autoplay: false,
      autoplaySpeed: 1500,
      slidesToShow: 4,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
    jQuery(".st-slider-kit").slick({
      infinite: false,
      speed: 1000,
      arrows: true,
      cssEase: "ease",
      autoplay: false,
      autoplaySpeed: 1500,
      slidesToShow: 1,
      slidesToScroll: 1
    });
    jQuery(".st-product-mens, .st-product-womens, .st-product-acc").slick({
      infinite: false,
      speed: 1000,
      cssEase: "ease",
      autoplay: false,
      autoplaySpeed: 1500,
      slidesToShow: 4,
      initialSlide: 0,
      slidesToScroll: 1,
      variableWidth: true,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
            arrows: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            centerMode: true,
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
    jQuery(".st-slider").slick({
      infinite: true,
      speed: 1000,
      arrows: false,
      cssEase: "ease",
      autoplay: true,
      autoplaySpeed: 2500,
      slidesToShow: 1,
      slidesToScroll: 1
    });

    // slider for kit detail
    jQuery(".slider-for").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      asNavFor: ".slider-nav",
      autoplay: false
    });
    jQuery(".slider-nav").slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      asNavFor: ".slider-for",
      dots: false,
      centerMode: false,
      focusOnSelect: true,
      arrows: false
    });

    // slider for media photo
    jQuery(".slider-media").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      asNavFor: ".slider-medianav",
      autoplay: false
    });
    jQuery(".slider-medianav").slick({
      slidesToShow: 8,
      slidesToScroll: 1,
      asNavFor: ".slider-media",
      dots: false,
      centerMode: false,
      focusOnSelect: true,
      arrows: false
    });

    // increase
    jQuery(".incr-btn").on("click", function (e) {
      var $button = jQuery(this);
      var oldValue = $button
        .parent()
        .find(".quantity")
        .val();
      $button
        .parent()
        .find('.incr-btn[data-action="decrease"]')
        .removeClass("inactive");
      if ($button.data("action") === "increase") {
        var newVal = parseFloat(oldValue) + 1;
      } else {
        // Don't allow decrementing below 1
        if (oldValue > 1) {
          var newVal = parseFloat(oldValue) - 1;
        } else {
          newVal = 1;
          $button.addClass("inactive");
        }
      }
      $button
        .parent()
        .find(".quantity")
        .val(newVal);
      e.preventDefault();
    });

    // contact animation
    jQuery(".contact-form")
      .find(".form-control")
      .each(function () {
        var targetItem = jQuery(this).parent();
        if (jQuery(this).val()) {
          jQuery(targetItem)
            .find("label")
            .css({
              top: "10px",
              fontSize: "14px"
            });
        }
      });
    jQuery(".contact-form")
      .find(".form-control")
      .focus(function () {
        jQuery(this)
          .parent(".input-block")
          .addClass("focus");
        jQuery(this)
          .parent()
          .find("label")
          .animate(
          {
            top: "10px",
            fontSize: "14px"
          },
          300
          );
      });
    jQuery(".contact-form")
      .find(".form-control")
      .blur(function () {
        if (jQuery(this).val().length === 0) {
          jQuery(this)
            .parent(".input-block")
            .removeClass("focus");
          jQuery(this)
            .parent()
            .find("label")
            .animate(
            {
              top: "25px",
              fontSize: "18px"
            },
            300
            );
        }
      });

    jQuery(".clickable-row").click(function() {
      window.location = jQuery(this).data("href");
    });

    // close match-banner
    jQuery(".match-banner").click(function() {});
    jQuery(".close").click(function(event) {
      event.stopPropagation();
      jQuery(".match-banner").fadeOut();
    })

    jQuery(window).scroll(function(){
			if (jQuery(this).scrollTop() > 80) {
				jQuery('header.header').addClass('reduced');
			} else {
				jQuery('header.header').removeClass('reduced');
			}
		});

  });
})(jQuery, window, document);

function edValueKeyPress() {
  var edValue = document.getElementById("edValue");
  var s = edValue.value;

  var lblValue = document.getElementById("generator");
  lblValue.textContent = s;
}
function numValueKeyPress() {
  var numValue = document.getElementById("numValue");
  var num = numValue.value;

  var numericValue = document.getElementById("generatorNum");
  numericValue.innerText = "text" + num;
}

/* mobile nav */
jQuery(function(){
  jQuery('#mobinav ul.top-nav li').each(function() {
    if(jQuery('.sub-menu',this).length) {
      jQuery('> a',this).addClass('toggle-submenu');
      jQuery('.sub-menu',this).each(function() {
        jQuery(this).addClass('level-'+jQuery(this).parents('#mobinav .sub-menu').length);
      });
    }
  });
  jQuery('#mobinav ul.monav li.has-submenu',this).append('<i class="fa fa-angle-down"></i>');

  //subnav toggle
  jQuery('ul.monav i').on('click', function(e){
    e.preventDefault();
    jQuery('#mobinav li').not(jQuery(this).parents('li')).removeClass('shrink');
    jQuery(this).closest('li').toggleClass('shrink');
  });
});
function openNav() {
  document.getElementById("mobinav").style.width = "100%";
}
function closeNav() {
  document.getElementById("mobinav").style.width = "0";
}
